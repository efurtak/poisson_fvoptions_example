This is just a basic example of how to solve Poisson's equation, using openFoam's laplacianFoam solver with fvOptions.  The forcing term is just a plane, which is defined in the topoSetDict.  There is no need to build a mesh - a meaningless one is included.  To run:

topoSet
laplacianFoam
paraFoam


Or just use the runit.sh script.
